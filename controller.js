(function() {
    'use strict';
  
    angular
      .module('eventbrite', [])
      .controller('eventbriteController', loadFunction);
  
    loadFunction.$inject = ['$scope', 'structureService', '$location', '$http', 'storageService'];
  
    function loadFunction($scope, structureService, $location, $http, storageService) {
      // Register upper level modules
      structureService.registerModule($location, $scope, 'eventbrite');
      // --- Start eventbriteController content ---
      var organizerId = $scope.eventbrite.modulescope.organizerId;
      var userToken = $scope.eventbrite.modulescope.personalToken;
      localStorage.eventBriteUserToken = userToken;
      console.log("////////////")
      console.log($scope.eventbrite.modulescope.childrenUrl.eventbrite)
      //API call to get events by user
      $http.get('https://www.eventbriteapi.com/v3/events/search/?organizer.id=' + organizerId + '&sort_by=date&token=' + userToken).then(function(data) {
          $scope.eventData = data.data.events;
          $scope.paintCalendar();
      })
      $scope.paintCalendar = function (){
          JSCalendar.on('new', function(cal, xtra) {
              _jscallog("Class level hook caught for instance with id " + cal.id)
          });
          var today = new Date();
          var matrix = {};
          matrix[today.getFullYear()] = {};
          var calendar = new JSCalendar(document.getElementById('myCalendar'), {
              titleCropSize : 30,
              eventBackground : "rgb(193, 155, 113)",
              width: "full",
              ampm: false
          });
          calendar.init();
  
          var todayEventTime = new Date();
          todayEventTime = new Date(todayEventTime.getFullYear(), todayEventTime.getMonth(), todayEventTime.getDate(), 10, 0, 0);
  
          var tomorrowEventTime = new Date(todayEventTime.getTime() + (1000 * 60 * 60 * 24));
          calendar.on('willRender', function() {
              var titleBar = calendar.elem.querySelector('.control-bar-title'); 
              titleBar.style.fontWeight = (
                  calendar.state.month == today.getMonth() && 
                  calendar.state.year  == today.getFullYear()
              ) ? "bold" : "";
  
          })
          for(event of $scope.eventData) {
              calendar.push({
                  at: event.end.local,
                  event : {
                      displayname : event.name.text,
                      color : "rgb(240, 85, 55)",
                      at : event.end.local,
                      duration : 1000 * 60 * 60 * 2
                  },
              })
          }
          calendar.render();
          var eventsToClick = document.getElementsByClassName("cell-event-mark");
          $scope.mountEvents(eventsToClick);
    }
    $scope.mountEvents = function (arrEvents) {
        console.log($scope.eventbrite.modulescope.childrenUrl)
        for(var i = 0; i < arrEvents.length; i++) {
            arrEvents[i].identifier1 = i
            arrEvents[i].addEventListener("click", function (event) {
                if(localStorage.eventSelected){
                    localStorage.eventSelected = JSON.stringify($scope.eventData[event.path[0].identifier1]);
                    window.location.replace('#' + $scope.eventbrite.modulescope.childrenUrl.eventbriteinfoevent);
                }
                else {
                    localStorage.eventSelected = JSON.stringify($scope.eventData[event.path[0].identifier1]);
                    console.log($scope.eventbrite.modulescope.childrenUrl.eventbriteinfoevent)
                    window.location.replace('#' + $scope.eventbrite.modulescope.childrenUrl.eventbriteinfoevent);
                }
            })
        }
    }
    }
}());
  